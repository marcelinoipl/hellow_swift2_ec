//
//  ViewController.swift
//  Hello_swift2
//
//  Created by Luis Marcelino on 19/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var helloLabel: UILabel!
    
    @IBOutlet weak var nombreEditText: UITextField!
    
    @IBAction func sayHelloActiion(sender: AnyObject) {
        let nombre:String? = "Luis"
        let hello = "Olá "
        
        print(nombre!)
        //let greeting = hello + " " + nombre!
        
        //let i = 1
        //let x = 1.5
        
        //helloLabel.text = String(Double(i) + x)
        let name = nombreEditText.text!
        helloLabel.text = hello + name
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let infoPessoal1 = (name: "Miguel", age: 20, nationality:
            "Portuguese", height: 1.23)
        
        print(infoPessoal1)
        
        var medidas = [Double] ()
        medidas.append(3.2)
        medidas.append(4.5)
        medidas.removeFirst()
        print(medidas[0])
        medidas = []
        medidas.removeAll()
        
        
        //        //let infoPessoal2 = ("Miguel", 20)
        //
        //        var weekend1:Array<String>
        //        var weekend2:[String]
        //
        //        var lista = ["leche", "pan","huevos"]
        let weekend = ["sabado","domingo"]
        
        var conta = 0
        
        for _ in weekend {
            //print(day)
            conta++
        }
        
        //        weekend.isEmpty
        //        weekend.count
        
        //        var euroRates1: Dictionary<String,Double>
        //        var euroRates2: [String:Double]
        
        var euroRates = ["USD":1.267, "GBP":0.780]
        
        for (currency, value) in euroRates {
            print("Euro rate to \(currency) is \(value)")
        }
        
        euroRates["BRL"] = 3.074
        euroRates["USD"] = 1.07
        
        let translation = ["Obrigado":"Thank you", "Por favor":"Please", "Bom dia":"Good morning"]
        
        let euros = 5.0
        let dollarRate = euroRates["USD"] //1.07
        
        
        var request = translation["Bom dia"]! + "!"
        
        let dollars = dollarRate! * euros
        
        request += String(format: "%.2f", dollars) + " dollars, "
        //let s = translation["gdurfg"]
        
        request += translation["Por favor"]! + "."
        
        print(request)
        
        euroRates.updateValue(1.08, forKey: "USD")
        euroRates["USD"] = 1.08
        
        euroRates.removeValueForKey("USD")
        euroRates["USD"] = nil
        
        //for loops
        
        for var i = 0; i<20; i++ {
            print(i)
        }
        
        for i in 1...20 {
            print(i)
        }
        
        for i in 0..<20 {
            print(i)
        }
        
        var i = 2
        let j = 3
        
        if i == j {
            
        } else {
            
        }
        
        var k:Int?
     
        
        
        if k == nil {
            print("optional is empty")
        } else {
            i = k!
        }
        
        
        if k != nil{
            i = k!
        }
        
        if let b = k {
            i = b
        }
        
        switch i {
        case 1:
            print("It is one!")
            fallthrough
        default:
            print("bigger than 1")
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

